let http = require('http');

let port = 4000;

let server = http.createServer(function(request, response) {
	if(request.url == '/' && request.method == 'GET'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Welcome to Booking System.');
	} else if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Welcome to your profile!');
	} else if(request.url == '/courses' && request.method == 'GET'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end(`Here's our courses available:`);
	} else if(request.url == '/addcourse' && request.method == 'POST'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Add a course to our resources.');
	} else {
		response.writeHead(404, {'Content-type': 'text/plain'});
		response.end('Page not found.');
	};
});

server.listen(port);

console.log(`Server is running at localhost:${port}`);

